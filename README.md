# Synthesize, Execute and Debug: Learning to Repair for Neural Program Synthesis

Этот репозиторий представляет собой повторение эксперимента из статьи

> Kavi Gupta, Peter Ebert Christensen*, Xinyun Chen*, Dawn Song, <cite> Synthesize, Execute and Debug: Learning to Repair for Neural Program Synthesis, in NeurIPS 2020. (* Equal contribution) </cite>

Статья [[arXiv](https://arxiv.org/abs/2007.08095)]  

## About data

Прежде чем входные данные начнут приносить пользу обществу в виде качественно синтезированных исполняемых программ, они проходят тернистый путь от файлов бинарного формата (.pkl) до батчей тренировочной выборки:

- При вызове функции `train` вызывается функция `train_start`, которая создает объекты обучающей и валидационной выборок
- В функции `train_start` вызывается функция `get_dataset` с аргиментами из командной строки (название датасета, на которым проводится эксперимент)
- В функции `get_dataset` вызывается функция `get_karel_dataset`
- В функции `get_karel_dataset` создается Dataloader (torch.utils.data.DataLoader) для обучения и валидации, которому на вход поступают аргументы:
  * Объект класса `KarelTorchDataset`
  * batch_size - аргумент командной строки
  * collate_fn - генератор батчей
  * num_workers - 0 если в командной строке указан `load_sync` и 4 в обратном случае
- `KarelTorchDataset` принимает на вход:
  * Путь к обучающей выборке (`../data/karel/train{}.pkl` или `../data/karel/val{}.pkl`)
  * mutator - объект класса `KarelExampleMutator`
  * incorrect_mutator - объект класса ` KarelOutputRefExampleMutator` (по умолчанию `None`)
  * replace_gold - объект класса `KarelGoldReplaceMutator`
- В классе `KarelTorchDataset` данные считываются из бинарного формата средствами python 3: `pickle.load(self.file, encoding='latin1')`

Таким образом, в функции `train` для обучения модели используются объекты `torch.utils.data.DataLoader`, в которых находятся данные, считанные из формата .pkl. 

## Setup

Исходный код написан на Python 3. Все необходимые зависимости представлены в файлах `requirements.txt` и `requirements2.txt`

1. Установка пакетов из  `requirements.txt`: `pip install -r requirements.txt`
2. Установка пакетов из  `requirements2.txt`: `pip install -r requirements2.txt`

## Training models

Эксперимент выполнен на основе датасета Karel:
```
cd data
wget https://s3.us-east-2.amazonaws.com/karel-dataset/karel.tar.gz
tar xf karel.tar.gz
```

### Synthesis Model

Обучение синтезатора программ (LRGL):
```
python3 train.py --dataset karel --model_type karel-lgrl \
  --num_epochs 100 --lr 1 --model_dir logdirs/synthesis
```

### Debugger Model

Создание данных для обучения отладчика. Создаются файлы  `lgrl_train_10.json`, который представляет собой образцы для обучения отладчика в формате .json и `lgrl_val_10.json` для валидации. Эти файлы в дальнейшем используются для finetuning-а отладчика. В исходном коде все входные  данные были переведены из бинарного формата (pickle) в json (в функции `run_predict`). В имплементации используется только 10% данных.

```
mkdir -p baseline

python3 eval.py --model_type karel-lgrl \
  --dataset karel --max_beam_trees 32 \
  --model_dir logdirs/synthesis \
  --run-predict --predict-path ../baseline/lgrl_train_10.json \
  --evaluate-on-all --eval-train

python3 eval.py --model_type karel-lgrl \
  --dataset karel --max_beam_trees 32 \
  --model_dir logdirs/synthesis \
  --run-predict --predict-path ../baseline/lgrl_val_10.json \
  --evaluate-on-all

```

Данные для finetuning-а отладчика модели `LGRL-GD`

```
python3 eval.py --model_type karel-lgrl \
  --dataset karel --max_beam_trees 1 \
  --model_dir logdirs/synthesis \
  --run-predict --predict-path ../baseline/lgrl_train_gd.json \
  --evaluate-on-all --eval-train

python3 eval.py --model_type karel-lgrl \
  --dataset karel --max_beam_trees 1 \
  --model_dir logdirs/synthesis \
  --run-predict --predict-path ../baseline/lgrl_val_gd.json \
  --evaluate-on-all

```


Обучение отладчика на мутированных данных
```
# with TraceEmbed
python3 train.py --dataset karel --model_type karel-lgrl-ref \
  --karel-mutate-ref --karel-mutate-n-dist 1,2,3 \
  --karel-trace-enc aggregate:conv_all_grids=True \
  --num_epochs 50  --lr 1 \
  --model_dir logdirs/debuggerTE

# without TraceEmbed
python3 train.py --dataset karel --model_type karel-lgrl-ref \
  --karel-mutate-ref --karel-mutate-n-dist 1,2,3 \
  --karel-trace-enc none \
  --num_epochs 50 --lr 1 \
  --model_dir logdirs/debuggerWOTE
```

Finetuning отладчика на выходах синтезатора моделей. Данные для обучения - формат json, 10% от исходных.
```
# with TraceEmbed
python3 train.py --dataset karel --model_type karel-lgrl-ref \
  --karel-file-ref-val ../baseline/lgrl_val_10.json --karel-file-ref-train ../baseline/lgrl_train_10.json \
  --pretrained entire-model::logdirs/debuggerTE --pretrained-step 436000 \
  --karel-trace-enc aggregate:conv_all_grids=True \
  --num_epochs 50 --lr 0.0001 \
  --model_dir logdirs/debuggerTE-finetuned

# without TraceEmbed
python3 train.py --dataset karel --model_type karel-lgrl-ref \
  --karel-file-ref-val ../baseline/lgrl_val_10.json --karel-file-ref-train ../baseline/lgrl_train_10.json \
  --pretrained entire-model::logdirs/debuggerWOTE --pretrained-step 436000 \
  --karel-trace-enc none \
  --num_epochs 50 --lr 0.0001 \
  --model_dir logdirs/debuggerWOTE-finetuned

```

Аналогичным образом для `LGRL-GD`:

```
# with TraceEmbed
python3 train.py --dataset karel --model_type karel-lgrl-ref \
  --karel-file-ref-val ../baseline/lgrl_val_gd.json --karel-file-ref-train ../baseline/lgrl_train_gd.json \
  --pretrained entire-model::logdirs/debuggerTE --pretrained-step 436000 \
  --karel-trace-enc aggregate:conv_all_grids=True \
  --num_epochs 50 --lr 0.0001 \
  --model_dir logdirs/debuggerTE-gd-finetuned

# without TraceEmbed
python3 train.py --dataset karel --model_type karel-lgrl-ref \
  --karel-file-ref-val ../baseline/lgrl_val_gd.json --karel-file-ref-train ../baseline/lgrl_train_gd.json \
  --pretrained entire-model::logdirs/debuggerWOTE --pretrained-step 436000 \
  --karel-trace-enc none \
  --num_epochs 50 --lr 0.0001 \
  --model_dir logdirs/debuggerWOTE-gd-finetuned

```

## Evaluating Debugger Model

Оценка качества полученной модели. Выход данной команды содержит:

- Total - количество примеров для валидации для каждого этапа
- Correct - количество программ, отрабатывающих на данных примерах верно
- Accuracy - точность предсказания (отношение количества программ, в точности совпадающих с программами из примера, к общему количеству программ для генерации для одного этапа). Accuracy в данном пункте не несет большой смысловой нагрузки, она будет всегда низкая, поскольку отладчик включается только в тех случаях, если синтезатор выдает неверный результат
- Syntax Errors - количество программ, сгенерированных с синтаксическими ошибками
- Runtime Exceptions - количество программ, упавших с `Runtime Exceptions`
- Text - исходная программа из примера для обучения
- Res - результат работы модели
- candidates - множество программ, сгенерированных моделью по примерам входных и выходных данных
- Stats - отчет по примеру для обучения. В идеальном случае выглядит так: `Stats: {'total': 6, 'correct': 6, 'exceptions': 0, 'result-none': 0, 'syntax-error': 0, 'runtime-exception': 0, 'individual': [1, 1, 1, 1, 1, 1]}`

```
python3 program_synthesis/eval.py --model_type karel-lgrl-ref \
  --dataset karel --max_beam_trees 64 \
  --iterative-search best_first --iterative-search-start-with-beams --evaluate-on-all
  --model_dir logdirs/debuggerTE \
  --karel-file-ref-val baseline/lgrl_val_10.json \
  --iterative-search-step-limit 25
```

В качестве валидации модели авторы предлагают использовать следующую команду:

```
python scripts/synthesizer_stats.py baseline/lgrl_val_10.json
```

В моем случае, вывод данной команды такой:

```{'correct': 2020, 'exact': 985, 'passes_given': 2129}```

Для `LGRL-GD`:

```{'exact': 921, 'correct': 1597, 'passes_given': 1663}```

Данная команда анализирует содержание файла `baseline/lgrl_val_10.json`, который представляет собой результат работы синтезатора (без отладчика) на валидационной выборке. Содержание анализируемого файла выглядит следующим образом:

- output - выход модели. Представляет собой совокупность программ, сгенерированных для примера входных данных
- total - количество программ для генерации в одном примере (в данной имплементации на вход поступает 6 примеров входных и выходных данных)
- correct - количество верно сгенерированных программ (в идеальном случае 6/6)
- exceptions - количество программ, которые на этапе выполнения упали с исключениями
- result-none - количество примеров, для которых программа не создалась
- syntax-error - количество сгенерированных программ с синтаксическими ошибками
- runtime-exception - количество программ, запустившихся с `runtime-exception`
- individual - отображает, какие из 6 программ сгенерировались верно (в идеальном случае `[1, 1, 1, 1, 1, 1]`)
- passes_given_tests - `true`, если все программы в примере сгенерировались верно и `false` в обратном случае

Программы, для которых `passes_given_tests = false`, поступают на вход к отладчику. 

Вывод скрипта:
- correct - сумма всех `correct` из `baseline/lgrl_val_10.json`
- exact - сумма программ из `output`, которые есть в примерах выхода для генерируемой модели
- passes_given - сколько программ прошло тесты

В валидационной выборке 2500 примеров, тесты прошли 2129, соответственно, на вход отладчику должен поступить 371 пример.

Для валидации отладчика использовались следующие команды:

```
python3 eval.py --model_type karel-lgrl-ref --dataset karel \
  --max_beam_trees 64 --iterative-search best_first \
  --iterative-search-start-with-beams --evaluate-on-all --model_dir logdirs/debuggerTE \
  --karel-file-ref-val ../baseline/lgrl_val_10.json --iterative-search-step-limit 25 \
  --run-predict --predict-path ../baseline/test_debuggerTE.json \
  --batch_size 16
```
Эта команда тестирует модель синтезатор+отладчик и записывает результаты валидации отладчика в файл `test_debuggerTE.json`. 

Прверить размер выхода:

```
with open('SED/baseline/test_debuggerTE.json') as f:
    test = json.loads(f.read())
print(len(test))

> 371
```
Действительно, 2129/2500 образцов валидационной выборки прошли тесты на этапе синтеза, а на вход отладчику должен отправиться 371 пример.

Проверка качества работы отладчика:

```
get_baseline_stats(path='SED/baseline/test.json', data_folder='SED/data')
```

Результат работы команды:

`{'correct': 94, 'exact': 0, 'passes_given': 112}`

Ни одна программа на выходе отладчика не совпала с примером из выборки, но 88 примеров из 371 отработали на входных данных корректно.

Таким образом, финальное качество модели синтезатор+отладчик будет `(94 + 2020) / 2500 = 0.8456`

Аналогичным образом тестировалось качество отладчика после finetuning-а, но результаты оказались хуже:

`{'correct': 88, 'exact': 0, 'passes_given': 104}`



Результаты повторения эксперимента:


| Model                          | Accuracy, %      | Total | Exact, % |
| -------------------------------|:----------------:| -----:| --------:|
| lgrl_only_synthesizer          | 0.8080           | 2500  | 0.394    |
| lgrl_TE                        | 0.8456           | 2500  | 0.394    |
| lgrl_WOTE                      | 0.8396           | 2500  | 0.394    |
| lgrl_TE_finetuned              | 0.8432           | 2500  | 0.394    |
| lgrl_WOTE_finetuned            | 0.8372           | 2500  | 0.394    |

Соответсвенно, ошибки моделей

|  Synthesizer	            |      LGRL       |     LGRL-GD     |
| -------------------------:|:---------------:|:---------------:|
| Without Debugger          |	19.20% (60.6%)	| 36.12% (63.16%) |
| No TraceEmbed+No Finetune	| 16.04% (60.6%)	| 32.96% (63.16%) |
| No TraceEmbed+Finetune	  | 16.28% (60.6%)	| 34.28% (63.16%) |
| TraceEmbed+No Finetune	  | 15.44% (60.6%)	| 32.36% (63.16%) |
| TraceEmbed+Finetune       |	15.68% (60.6%)	| 32.92% (63.16%) |

Результаты авторов

|  Synthesizer	            |      LGRL       |     LGRL-GD     |
| -------------------------:|:---------------:|:---------------:|
| Without Debugger          |	22.00% (63.40%)	| 39.00% (65.72%) |
| No TraceEmbed+No Finetune	| 14.88% (62.72%)	| 18.56% (62.84%) |
| No TraceEmbed+Finetune	  | 14.32% (62.48%)	| 16.12% (60.88%) |
| TraceEmbed+No Finetune	  | 14.60% (62.88%)	| 18.68% (63.84%) |
| TraceEmbed+Finetune       |	14.28% (62.68%)	| 16.12% (61.16%) |

По результатам сравнения видно, что при повторении эксперимента синтезатор обучился лучше и показывает более высокое качество, чем заявлено у авторов статьи. Но поскольку данные для обучения отладчика были сокращены, то повторение эксперимента показывает ожидаемое понижение качества. 

